# variables

variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "key_name" {}
variable "private_key_path" {}
variable "instance_user" {}


variable "region" {
  default = "us-east-1"
}

# configure aws provider to connect to
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.region
}

# gather aws ami information
data "aws_ami" "aws-linux" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["amzn-ami-hvm*"]
  }

  filter {
    name = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]

  }

}

# use the default vpc, this wont get destroyed on delete
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

# create security group to allow ssh using vpc defined above
resource "aws_security_group" "allow_ssh" {
  name = "nginx_demo"
  description = "allow ports for nginx demo"
  vpc_id = aws_default_vpc.default.id
 
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# create ec2 instance using security group defined above
resource "aws_instance" "nginx" {
  ami = data.aws_ami.aws-linux.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.key_name

  # establish connection for provisioner
  connection {
    type = "ssh"
    host = self.public_ip
    user = var.instance_user
    private_key = file(var.private_key_path)    
  }
  
  # run commands to install and start nginx
  provisioner "remote-exec" {
    inline = [
      "sudo yum install nginx -y",
      "sudo service nginx start"
    ]
  }

}

# output the fqdn to connect to
output "aws_instance_public_dns" {
  value = aws_instance.nginx.public_dns
}


