# Terraform Nginx Example
> This serves as a referencable terraform setup for a nginx server in an AWS EC2 instance;



## Files
---
`main.tf` - this hosts the entire configuration, can be split up but we are keeping it all together here for easier reference

`terraform.tfvars` - this file contains variables that are used within the main terraform config.
> `terraform.tfvars.example` is included to help create this file
```
# IAM Access and Secret Key for your IAM user
aws_access_key = "ABCD1234"
aws_secret_key = "ABCD78910"

# Key name for connction to ec2 instance
key_name = "nginx-key"
instance_user = "ec2-user" # centos ami default user

# path to private key used to connect to ec2 instance
private_key_path = "/path/to/nginx-key.pem"
```

`key_name.pem` - you will need to create a key/pair within aws that will be used for the instance, this is not included but should be known prior to continuing.
> A key can be created in AWS console EC2>Network&Security>Key Pairs


## Running
--

`terraform init` - this will initialize terraform and pull required providers for access to aws

`terraform validate` - validate terraform configuration files

`terraform plan` - used to output the plan/steps required to reach desired state within terraform config
```
# outputting plan to file
terraform plan -out <file_name>

# example
terraform plan -out nginx.tfplan
```

`terraform apply` - used to execute steps in the terraform plan to achieve desired configurations
```
# this will create a plan and apply at the same time
terraform apply

# this will apply a previously output plan
terraform apply <file_name>

# example
terraform apply nginx.tfplan

```

## References
---
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/connection-prereqs.html
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs
- https://github.com/ned1313/Getting-Started-Terraform/tree/pre-1.0


